from snakemake.utils import report
import os

preprocessing_methods = ["first", "second"]
model_types = ["1", "2"]

rule all:
    input:
        expand("models/model_pre_{preprocessing_method}_train_{model_type}.joblib", preprocessing_method=preprocessing_methods, model_type=model_types)

rule read_data:
    input:
        "data/iris.csv"
    output:
        "data/data.csv"
    shell:
        "cat {input} > {output}"

rule preprocess_data:
    input:
        "data/data.csv"
    output:
        "data/preprocessed_data_{preprocessing_method}.csv"
    script:
        "scripts/preprocess_{wildcards.preprocessing_method}.py"

rule train_model:
    input:
        "data/preprocessed_data_{preprocessing_method}.csv"
    output:
        "models/model_pre_{preprocessing_method}_train_{model_type}.joblib"
    threads: int((os.cpu_count() + 1) / 2)
    script:
        "scripts/train_model{wildcards.model_type}.py"








# X = ['x', 'y']
# Y = ["train", "test"]

# rule read_data:
#     input:
#         data = "iris.csv"

# rule preprocess:
#     input:
#         preprocess_input = rules.read_data.input
#     output:
#         X_train = 'X_train.csv',
#         X_test = 'X_test.csv',
#         y_train = 'y_train.csv',
#         y_test ="y_test.csv"
#     script:
#         "scprit/preprocess.py"






# X = ['x', 'y']
# Y = ["train", "test"]

# rule all:
    # input:
        # df = "{prefix}.csv"
        # features_target = expand("{x}_{y}.csv", x=X, y=Y)
    # shell:
        # "echo {wildcards.prefix}"
    # script:
        # "scripts/test.py"
# rule create_files:
#     output:
#         expand("{x}_{y}.txt", x=X, y=Y)
#     shell:
#         "touch {output}"


# rule sort_data:
#     input:
#         df_train = '{prefix}.csv'
#     output:
#         df_cor = 'df_cor.csv'
#     shell:
#         'echo {wildcards.prefix} > output.txt'
    # script:
    #     "scripts/test.py"
# rule train_model:
#     input:





# import pandas as pd
# rule test:
#     input:
#         data = 'iris.csv'
#     run:
#         df = pd.read_csv(input.data)

# rule sort:
#     input:
#         df_to_sort = rules.test.run.df
#     output:
#         sorted_file ='results/processed_iris.csv'
#     script:
#         "scripts/test.py"


    # output: "iris_output.csv"
# rule world:
#     input: 'output.txt'
#     output: 'second.txt'
#     shell:
#         """
#         cat {input} > {output}
#         echo This is the second line >> {output}
#         """
