import pandas as pd
import snakemake

snake = snakemake
data = pd.read_csv(snake.input[0])
data["target"] = data["target"].astype("int")
for col in data.drop("target", axis=1).columns:
    data[col].fillna(data[col].median())
data.to_csv(snake.output[0], index=False)
