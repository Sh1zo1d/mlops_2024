import pandas as pd
import snakemake
from joblib import dump
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

snake = snakemake
data = pd.read_csv(snake.input[0])
data["target"] = data["target"].astype("int", errors="ignore")
features = data.drop("target", axis=1)
target = data["target"]

features_train, features_test, target_train, target_test = train_test_split(
    features, target, test_size=0.3
)
min_max_scaler = MinMaxScaler()

features_train_scaled = min_max_scaler.fit_transform(features_train)
features_test_scaled = min_max_scaler.transform(features_test)

lr_model = LogisticRegression()
lr_model.fit(features_train_scaled, target_train)
f1 = f1_score(target_test, lr_model.predict(features_test_scaled), average="weighted")
print(f"F1-Score: {f1}")
dump(lr_model, snake.output[0])
