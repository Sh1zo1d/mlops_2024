import pandas as pd
import snakemake

snake = snakemake
data = pd.read_csv(snake.input[0])
data["target"] = data["target"].astype("int")
data.dropna(axis=0, inplace=True)
data.to_csv(snake.output[0], index=False)
# import pandas as pd
# from sklearn.model_selection import train_test_split
# from sklearn.preprocessing import MinMaxScaler

# # Read data
# data = pd.read_csv(snakemake.read_data.input.data)
# data['target'] = data['target'].astype('int', errors='ignore')
# features = data.drop('target', axis=1)
# target = data['target']

# # Train data
# features_train, features_test, target_train, target_test = train_test_split(features, target, test_size=0.3)
# min_max_scaler = MinMaxScaler()
# features_train_scaled = min_max_scaler.fit_transform(features_train)
# features_test_scaled = min_max_scaler.transform(features_test)

# # Save preprocessed data
# features_train.to_csv(snakemake.preprocess.output.X_train, index=False)
# features_test.to_csv(snakemake.preprocess.output.X_test, index=False)
# target_train.to_csv(snakemake.preprocess.output.y_train, index=False)
# target_test.to_csv(snakemake.preprocess.output.y_test, index=False)
