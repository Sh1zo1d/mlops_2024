from setuptools import find_packages, setup

setup(
    name="test-package",
    version="1.0.0",
    author="sh1zo1d",
    author_email="work_mail_94@mail.ru",
    description="A demo py package to deploy on gitlab",
    license="MIT",
    packages=find_packages(),
    classifiers=["Python 3.10.4", "GitLab"],
)
