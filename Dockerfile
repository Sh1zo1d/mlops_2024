FROM python:3.10.4

WORKDIR /Projects
COPY . .
RUN python -m pip install --no-cache-dir poetry==1.8.2 \
    && poetry config virtualenvs.in-project true \
    && poetry install --without dev,prod --no-interaction --no-ansi
ENTRYPOINT [ "poetry","run", "python","example.py" ]
